#!/usr/bin/python3
# -*- coding: utf-8 -*-

import gordon_voice

# Enable logging
gordon_voice.enableLogging()

# Instanciate gordon_voice
g = gordon_voice.GordonVoice()
# Load the modules
g.loadModules()

g.tts("I will give you the elapsed time since midnight every 30 seconds.")
g.tts("To stop the program just say 'exit'")
g.tts("Please ask me 'how many times ?'")

keep = True
while keep:
	# Attempt to get a notification
    n = g.notifier.getNotification()
    if n:
		# If there's a modification, say it
        g.tts(n)
    try:
		# Try to recognize text for speech
        text = g.activeListen("",{'timeout':1.5}).lower()
    except gordon_voice.SafeExit as e:
		# If we said the exit word
        g.tts("Bye !")
        keep = False
    except Exception as e:
        print(str(e))
    else:
		# If we could recognize something
        if text:
            try:
				# Try to get a module to recognize the input
				# can throw gordon_voice.ModuleHandleError if no module recognized the input
                moduleName,moduleFunc = g.handle(text)
            except:
                g.tts("I don't know what to do with this.")
            else:
                try:
					# Call the module's handle function
                    r = moduleFunc()
                except Exception as e:
                    print(str(e))
