# -*- coding: utf-8 -*-

WORDS = ["times"]

def isValid(text):
    return "how many times" in text

def handle(profile,activeListen,tts,moduleData,changeModuleData,text):
    nb = moduleData if isinstance(moduleData,int) and moduleData >= 0 else 0

    tts("You called me "+str(nb)+" times.")

    changeModuleData(nb+1)
