# -*- coding: utf-8 -*-

import datetime

FREQUENCY = 30 # 30s

def processNumber(n):
	if n < 10 :
		return str("0"+str(n))
	return str(n)

# Will be called every 30s (will queue a time notification)
def handle(timestamp,profile,qFunc):
    now = datetime.datetime.now()
    qFunc(processNumber(now.hour)+":"+processNumber(now.minute)+":"+processNumber(now.second))
    return now.second
