To launch the example :
* Place the content of example/ in the same directory as the gordon_voice package (or just ln -s ../gordon_voice from the example directory)
* Install the dependencies : `sudo apt-get install portaudio19-dev ffmpeg && pip3 install -r requirements.txt && pip3 install google_speech && sudo apt-get install sox libsox-fmt-mp3`
* Launch example.py
* Say "How many times" , multiple times
* Say "Exit" to exit the example
