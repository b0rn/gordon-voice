# gordon-voice

gordon_voice enables you to create an application that will recognize speech and
perform specific tasks depending on what the user said. This library can also perform text to speech.

You can choose between multiple APIs to perform Speech To Text (STT) or Text To Speech (TTS).

gordon_voice is greatly inspired by [Jasper](https://jasperproject.github.io/). In fact, gordon_voice relies on user defined python modules.

You can create two types of modules :
* Standard module : a python module which has a isValid function (that determines if the input is accepted by the module), and a hanle function (that performs a task depending on the input).
* Notification module : a python module which has a handle function that will fetch informations and queue its notification in the notification system. All notification modules also have a FREQUENCY attribute that defines the number of seconds between each call to their handle function. The notification system runs in its own thread.

To learn more, see the example in the `example` directory and visit the [documentation](https://b0rn.gitlab.io/gordon-voice/).



# Requirements
* python >= 3.4
* python3-dev
* portaudio19-dev
* pyaudio
* SpeechRecognition
* pyyaml

# Install
```
sudo apt-get install -y portaudio19-dev python3-dev ffmpeg && pip3 install -r requirements.txt
```

# Speech to text APIs
* [sphinx](https://cmusphinx.github.io/)
* [google](http://www.chromium.org/developers/how-tos/api-keys)
* [google_cloud](https://cloud.google.com/speech-to-text/docs/reference/libraries#client-libraries-install-python) (Follow the Python installation guide and authentification setup)
* [wit](https://wit.ai/)
* [houndify](https://www.houndify.com/)
* [ibm](https://www.ibm.com/watson/services/speech-to-text/)

# Text to Speech APIs
TTS APIs are optional, if you want to be able to use an API you have to install it and set it up.

* [gTTS](https://github.com/pndurette/gTTS)
* [google_speech](https://github.com/b0rn/GoogleSpeech)
* [google-cloud](https://cloud.google.com/text-to-speech/docs/reference/libraries#installing_the_client_library) (Follow the Python installation guide and authentification setup)
* [espeak](http://espeak.sourceforge.net/download.html) (or sudo apt-get install espeak on Linux)

# TODO
* Upload the library to PiPy

# gordon_voice licence
MIT License

Copyright (c) 2019 [Victor Leveneur (b0rn)](https://gitlab.com/b0rn)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


# PyAudio licence
PyAudio v0.2.11: Python Bindings for PortAudio.

Copyright (c) 2006 Hubert Pham

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

# SpeechRecognition licence
Copyright 2014-2017 Anthony [Zhang (Uberi)](http://anthonyz.ca/). The source code for this library is available online at [GitHub](https://github.com/Uberi/speech_recognition).

SpeechRecognition is made available under the 3-clause BSD license. See LICENSE.txt in the project’s [root directory](https://github.com/Uberi/speech_recognition) for more information.

For convenience, all the official distributions of SpeechRecognition already include a copy of the necessary copyright notices and licenses. In your project, you can simply say that licensing information for SpeechRecognition can be found within the SpeechRecognition README, and make sure SpeechRecognition is visible to users if they wish to see it.

SpeechRecognition distributes source code, binaries, and language files from [CMU Sphinx](http://cmusphinx.sourceforge.net/). These files are BSD-licensed and redistributable as long as copyright notices are correctly retained. See speech_recognition/pocketsphinx-data/*/LICENSE*.txt and third-party/LICENSE-Sphinx.txt for license details for individual parts.

SpeechRecognition distributes source code and binaries from [PyAudio](http://people.csail.mit.edu/hubert/pyaudio/). These files are MIT-licensed and redistributable as long as copyright notices are correctly retained. See third-party/LICENSE-PyAudio.txt for license details.

SpeechRecognition distributes binaries from [FLAC](https://xiph.org/flac/) - speech_recognition/flac-win32.exe, speech_recognition/flac-linux-x86, and speech_recognition/flac-mac. These files are GPLv2-licensed and redistributable, as long as the terms of the GPL are satisfied. The FLAC binaries are an [aggregate](https://www.gnu.org/licenses/gpl-faq.html#MereAggregation) of [separate programs](https://www.gnu.org/licenses/gpl-faq.html#NFUseGPLPlugins), so these GPL restrictions do not apply to the library or your programs that use the library, only to FLAC itself. See LICENSE-FLAC.txt for license details.
