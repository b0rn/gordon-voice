# -*- coding: utf-8 -*-

#####################
#				    #
#	02/12/2018		#
#	Victor Leveneur #
#					#
#####################

import speech_recognition as sr

try: from gtts import gTTS
except: gTTS = None

try: from google_speech import Speech as gSpeech
except : gSpeech = None

try: from google.cloud import texttospeech as gcTTS
except: gcTTS = None

import base64
import os
import uuid
import tempfile
from threading import RLock
from . import audiosetup

class GoogleCloudTTSError(Exception):
	pass
class GTTSError(Exception):
	pass
class GoogleSpeechError(Exception):
	pass
class APIImportError(Exception):
	pass
class WriteMP3Error(Exception):
	pass

class Speech:
	"""
	A class to handle Text to Speech (TTS) and Speech to Text (STT).

	Attributes
	----------
	recognizer : speech_recognition.Recognizer
		Instance of Recognizer. Used to perform STT.
	"""
	def __init__(self):
		self.recognizer = sr.Recognizer()
		self.lock = RLock()
		self.playingTTS = False

	def stt(self,language,timeout=None,device=None,sampleRate=16000,chunkSize=1024,
			adjustDuration=0.8,api='sphinx',keywords=None,googleKey=None,
			googleCloudJSON=None,witKey='',bingKey='',houndifyIDs=('',''),ibmIDs=('','')):

			"""
			Performs Speech To Text (STT).

			Will transform speech to text using one of the available APIs (sphinx,ibm,wit,bing,google,
			google-cloud,houndify). You can find more information `here <https://github.com/Uberi/speech_recognition/blob/master/reference/library-reference.rst>`_. by looking at the
			recognize_* functions.

			Parameters
			----------
				language : str
					The language tag of the audio to translate to speech.

					* sphinx : RFC5646 language tag like "en-US" or "en-GB"
					* google : IETF language tag like "en-US" or "en-GB"
					* google-cloud : BCP-47 language tag like "en-US"
					* wit : the language has to be set on Wit.ai app settings
					* bing : BCP-47 language tag like "en-US"
					* houndify : currently, only english is supported
					* ibm : RFC5646 language tag with a dialect like "en-US"

					Essentially, the language tag will be like "en-US".

					`Installing other languages for Sphinx <https://github.com/Uberi/speech_recognition/blob/master/reference/pocketsphinx.rst>`_.
				timeout : float
					Optional.

					Maximum number of seconds to wait for a phrase before throwing speech_recognition.WaitTimeoutError
					exception.

					Default is None : there will be no wait timeout.
				device : int
					Optional.

					Index of the input device to use.

					Look at the `audiosetup` module for more information.

					Default is the default input device.
				sampleRate : int
					Optional.

					Samples recorded per seconds (Hertz). Higher sampleRate result in better audio but also more
					bandwidth, thus, slower recognition.

					Default is 16000 Hz.
				chunkSize : int
					Optional.

					Sample size. Higher sample size avoid triggering on rapidly changing ambient noise,
					but also makes detection less sensitive. Generally should be left at its default.

					Default is 1024.
				adjustDuration : float
					Optional.

					The maximum number of seconds to adjust the microphone's threshold.
					This value should be at least 0.5 in order to get a representative sample of the ambient noise.

					Default is 0.8s.
				api : str
					Optional.

					Api to use. You can use sphinx,google,google-cloud,wit,ibm,bing or houndify.

					+--------------+---------+-------------------------------------------------------+--------------+
					| API          | Type    | Quota                                                 | Pricing      |
					+--------------+---------+-------------------------------------------------------+--------------+
					| sphinx       | Offline | None                                                  | Free         |
					+--------------+         +                                                       +              +
					| wit          |         |                                                       |              |
					+--------------+---------+-------------------------------------------------------+              +
					| google       | Online  | 50 requests per day                                   |              |
					+--------------+         +-------------------------------------------------------+--------------+
					| google-cloud |         | 60 minutes (free plan), 1 million minutes (paid plan) | Free/Premium |
					+--------------+         +-------------------------------------------------------+              +
					| ibm          |         | 100 minutes (free plan)                               |              |
					+--------------+         +-------------------------------------------------------+              +
					| houndify     |         | Unknown                                               |              |
					+--------------+         +-------------------------------------------------------+              +
					| bing         |         | 5 hours/month (free plan)                             |              |
					+--------------+---------+-------------------------------------------------------+--------------+

					More information :
						* `sphinx <https://cmusphinx.github.io/>`_.
						* `google <http://www.chromium.org/developers/how-tos/api-keys>`_.
						* `google-cloud <https://cloud.google.com/speech-to-text/>`_.
						* `wit <https://wit.ai/>`_.
						* `ibm <https://www.ibm.com/watson/services/speech-to-text/>`_.
						* `houndify <https://www.houndify.com/>`_.
						* `bing <https://azure.microsoft.com/en-ca/pricing/details/cognitive-services/speech-services/>`_.

					Default is sphinx.
				keywords : Iterable[Tuple[str, float]] or Iterable[str]
					Optional.

					If the sphinx API is used then it is an iterable of (keyword,sensitivity) where keyword is a phrase
					, and sensitivity is how sensitive to this phrase the recognizer should be, on a scale of 0
					(very insensitive, more false negatives) to 1 (very sensitive, more false positives) inclusive.
					Specifying keywords is more accurate than just looking for those same keywords in non-keyword-based
					transcriptions, because Sphinx knows specifically what sounds to look for. If unspecified,
					Sphinx will simply transcribe whatever words it recognizes.

					If the google-cloud API then keywords is an iterable of phrase strings, those given phrases will be
					more likely to be recognized over similar-sounding alternatives. Note that the API imposes certain
					`restrictions on the list of phrase strings <https://cloud.google.com/speech/limits#content>`_.

					Example :
						For Sphinx : keywords=[("one", 1.0), ("two", 1.0), ("three", 1.0)]
						For Google-cloud : keywords=["one"]
				googleKey : str
					Optional.

					The google API key to use.

					If google API is used and the key is undefined then the
					speech_recognition's default key will be used (this is not recommended).
					`More information <https://github.com/Uberi/speech_recognition/blob/master/reference/library-reference.rst#recognizer_instancerecognize_googleaudio_data-audiodata-key-unionstr-none--none-language-str--en-us--pfilter-union0-1-show_all-bool--false---unionstr-dictstr-any>`_.
				googleCloudJSON : str
					Optional.

					The content of Google cloud API's JSON credentials file.

					If not specified, speech_recognition will try to automatically find the default API credentials JSON file.
					`More information <https://github.com/Uberi/speech_recognition/blob/master/reference/library-reference.rst#recognizer_instancerecognize_google_cloudaudio_data-audiodata-credentials_json-unionstr-none--none-language-str--en-us-preferred_phrases-unioniterablestr-none--none-show_all-bool--false---unionstr-dictstr-any>`_.
				witKey : str
					Optional.

					The Wit.ai API key.
					`More information <https://github.com/Uberi/speech_recognition/blob/master/reference/library-reference.rst#recognizer_instancerecognize_witaudio_data-audiodata-key-str-show_all-bool--false---unionstr-dictstr-any>`_.
				bingKey : str
					Optional.

					The Microsoft Bing Speech API key.
					`More information <https://github.com/Uberi/speech_recognition/blob/master/reference/library-reference.rst#recognizer_instancerecognize_bingaudio_data-audiodata-key-str-language-str--en-us-show_all-bool--false---unionstr-dictstr-any>`_.
				houndifyIDs : Tuple[str,str]
					Optional.

					The Houndify client ID and client key a tuple : (cliendId,clientKey).
					`More information <https://github.com/Uberi/speech_recognition/blob/master/reference/library-reference.rst#recognizer_instancerecognize_houndifyaudio_data-audiodata-client_id-str-client_key-str-show_all-bool--false---unionstr-dictstr-any>`_.
				ibmIDs : Tuple[str,str]
					Optional.

					The IBM Speech to Text username and password as a tuple : (username,password).
					`More information <https://github.com/Uberi/speech_recognition/blob/master/reference/library-reference.rst#recognizer_instancerecognize_ibmaudio_data-audiodata-username-str-password-str-language-str--en-us-show_all-bool--false---unionstr-dictstr-any>`_.
			Returns
			-------
			string
				The interpretation of the speech.

			Raises
			------
			speech_recognition.WaitTimeoutError
				If no speech is detected before `timeout`.
			speech_recognition.RequestError
				If there is a problem with the API used (installation,authentification,operation or internet connection).
			speech_recognition.UnknownValueError
				If the speech is unintelligible.
			"""
			with sr.Microphone(device_index=device,sample_rate=sampleRate,chunk_size=chunkSize) as source:
				self.recognizer.adjust_for_ambient_noise(source,duration=adjustDuration)
				try:
					if not self.playingTTS:
						audio = self.recognizer.listen(source,timeout=timeout)
					else:
						return
				except:
					raise

			try:
				if api == 'sphinx':# keywords = [('words',sensitivity)]
					return self.recognizer.recognize_sphinx(audio, language=language
							,keyword_entries=keywords).strip()
				elif api == 'google':
					return self.recognizer.recognize_google(audio,
							language=language,key=googleKey).strip()
				elif api == 'google-cloud': # keywords = ['words']
					return self.recognizer.recognize_google_cloud(audio,credentials_json=googleCloudJSON
							,language=language,preferred_phrases=keywords).strip()
				elif api == 'wit':
					return self.recognizer.recognize_wit(audio,key=witKey).strip()
				elif api == 'bing':
					return self.recognizer.recognize_bing(audio,key=bingKey,language=language).strip()
				elif api == 'houndify':
					return self.recognizer.recognize_houndify(audio,client_id=houndifyIDs[0]
							,client_key=houndifyIDs[1]).strip()
				elif api == 'ibm':
					return self.recognizer.recognize_ibm(audio,username=ibmIDs[0],password=ibmIDs[1]
							,language=language).strip()
			except:
				raise

	def tts(self,text,device=None,lang='en-US',api='espeak',espeakArgs={},googleCloudArgs={}):

		"""
		Performs Text to Speech (TTS).

		Will perform TTS using one of the available APIs.

		Parameters
		----------
		text : str
			The text to convert to speech.
		lang : str
			Optional.

			The BCP-47 language tag to use (like "en-US" for english).

			Default is english.
		api : str
			Optional.

			Api to use. You can choose espeak, google-cloud or gTTS.

			* | espeak : Free offline TTS. You have to install espeak for this API to work. If you're using Windows, you'll have to add the espeak directory to
			  | your PATH environment variable.
			* | google-cloud : Online TTS. There's a free plan and a premium plan. `More information <https://cloud.google.com/text-to-speech/#cloud-text-to-speech-pricing>`_.
			  | Please follow `this setup <https://cloud.google.com/text-to-speech/docs/reference/libraries>`_ for this API to work.
			* gTTS : Free online TTS. Using google translate's voice.
			* google_speech : Free online TTS. Using google translate's voice. Faster than gTTS.

			Default is espeak.
		device : int
			Optional.

			The index of the output device.

			Look at the `audiosetup` module for more information.

			Default is None (the default output device will be used).
		espeakArgs : dict
			Optional.

			Additional settings for the espeak API.

			+-----+----------------------------------------+---------+
			| Key | Description                            | Default |
			+-----+----------------------------------------+---------+
			| a   | Volume (0-200)                         | 100     |
			+-----+----------------------------------------+---------+
			| p   | Pitch (0-99)                           | 50      |
			+-----+----------------------------------------+---------+
			| s   | Words-per-minute                       | 175     |
			+-----+----------------------------------------+---------+
			| g   | Pause between words (in units of 10ms) |   0     |
			+-----+----------------------------------------+---------+
			| v   | Voice to use.                          |   en    |
			+-----+----------------------------------------+---------+

			Visit `the espeak "-v" parameter <http://espeak.sourceforge.net/commands.html>`_ to see the available voices.
		googleCloudArgs : dict
			Optional.

			Additional settings for the google-cloud API.

			+--------------+-------------------------------------------------+---------+
			| Key          | Description                                     | Default |
			+--------------+-------------------------------------------------+---------+
			| name         | The name of the voice.                          | None    |
			+--------------+-------------------------------------------------+---------+
			| speakingRate | Speaking rate,in the range [0.25, 4.0].         | 1.0     |
			+--------------+-------------------------------------------------+---------+
			| pitch        | Speaking pitch,in the range [-20.0, 20.0].      | 0.0     |
			+--------------+-------------------------------------------------+         +
			| volumeGainDb | Volume gain (in dB),in the range [-96.0, 16.0]. |         |
			+--------------+-------------------------------------------------+---------+

			For more information visit the `google-cloud TTS documentation <https://cloud.google.com/text-to-speech/docs/reference/rest/v1/text/synthesize>`_.

		Raises
		------
		:exc:gordon_voice.GoogleSpeechError`
			If google_speech encountered an error while trying to perform TTS.
		:exc:`gordon_voice.GTTSError`
			If gTTS encountered an error while trying to perform TTS.
		:exc:`gordon_voice.GoogleCloudTTSError`
			If google-cloud TTS api encountered an error. It could be either an authentification error or a connection issue.
		:exc:`gordon_voice.WriteMP3Error`
			If the MP3 file could not be written.
		:exc:`gordon_voice.WAVFileError`
			If there's an error while trying to play the audio file.
		:exc:`gordon_voice.APIImportError`
			If the API isn't installed.
		"""

		tmpFile_name = tempfile.gettempdir()+'/'+str(uuid.uuid4())+'_gordon_tts'
		if api == 'espeak':
			# On windows : add the espeak cli directory in PATH
			defaultArgs = {'a':100,'p':50,'s':175,'g':0,'v':'en'}
			args = {k : v for d in [defaultArgs,espeakArgs] for k,v in d.items()}
			tmpFile_name += '.wav'
			os.system('espeak -a {0} -p {1} -s {2} -g {3} -v {4} -w {5} "{6}"'.format(args['a'],args['p'],args['s'],args['g']
			,args['v'],tmpFile_name,text.replace('"','')))
		elif api == 'google-cloud': # Online
			if gcTTS:
				# Must setup google-cloud auth
				# https://cloud.google.com/text-to-speech/docs/reference/libraries#client-libraries-usage-python
				client = gcTTs.TextToSpeechClient()

				defaultArgs = {'name':'','speakingRate':1.0,'pitch':0.0,'volumeGainDb':0.0}
				args = {k : v for d in [defaultArgs,espeakArgs] for k,v in d.items()}

				synthesis_input = texttospeech.types.SynthesisInput(text=text)
				voiceArgs = {'language_code':lang,ssml_gender:texttospeech.enums.SsmlVoiceGender.NEUTRAL}
				if args['name']:
					voiceArgs['name'] = args['name']
				voice = texttospeech.types.VoiceSelectionParams(**voiceArgs)
				audio_config = texttospeech.types.AudioConfig(
	    						audio_encoding=texttospeech.enums.AudioEncoding.MP3,
								speakingRate=args['speakingRate'],pitch=args['pitch'],
								volumeGainDb=args['volumeGainDb'])
				try:
					response = base64.decodebytes(client.synthesize_speech(synthesis_input, voice, audio_config))
				except Exception as e:
					raise GoogleCloudTTSError("Could not perform Google-cloud TTS : "+str(e))
				try:
					tmpFile_name += '.mp3'
					with open(tmpFile_name,'wb') as f:
						f.write(response)
				except Exception as e:
					raise WriteMP3Error("Could not save mp3 file : "+str(e))
			else:
				raise APIImportError("Cannot use google-cloud API : module not loaded.")
		elif api == 'gTTS': # Online (google translate)
			if gTTS:
				try:
					response = gTTS(text, lang=lang)
				except Exception as e:
					raise GTTSError("gTTS exception : "+str(e))
				else:
					tmpFile_name += '.mp3'
					try:
						response.save(tmpFile_name)
					except Exception as e:
						raise WriteMP3Error("Could not save mp3 file : "+str(e))
			else:
				raise APIImportError("Cannot use gTTS API : module not loaded.")
		elif api == 'google_speech':
			if gSpeech:
				try:
					speech = gSpeech(text,lang)
				except Exception as e:
					raise GoogleSpeechError("google_speech exception :"+str(e))
				else:
					tmpFile_name += '.mp3'
					try:
						speech.save(tmpFile_name)
					except Exception as e:
						raise WriteMP3Error("Could not save mp3 file : "+str(e))
			else:
				raise APIImportError("Cannot use google_speech API : module not loaded.")
		else:return

		if tmpFile_name.endswith('.mp3'):
			# On windows : install ffmpeg and add it to PATH
			# Convert mp3 to wav
			wavName = tmpFile_name[:len(tmpFile_name)-len('.mp3')]+'.wav'
			os.system("ffmpeg -i "+tmpFile_name+" "+wavName)
			# Remove mp3 file
			os.remove(tmpFile_name)
			# Remember the wav file
			tmpFile_name = wavName

		self.playingTTS = True
		audiosetup.play(tmpFile_name,device)
		self.playingTTS = False
		os.remove(tmpFile_name)
