import logging.config
from os import path
from .GordonVoice import GordonVoice,NoModuleHandleError,SafeExit
from .Speech import GoogleCloudTTSError,GTTSError,GoogleSpeechError,APIImportError,WriteMP3Error
from .audiosetup import WAVFileError

def enableLogging():
    '''
    To enable logging. The output file will be in the directory where the main program is executed.
    '''
    log_file_path = path.join(path.dirname(path.abspath(__file__)), 'logging.conf')
    logging.config.fileConfig(fname=log_file_path, disable_existing_loggers=False)
