# -*- coding: utf-8 -*-

#####################
#				    #
#	13/12/2018		#
#	Victor Leveneur #
#					#
#####################

from threading import Thread
import time
import queue
from os.path import basename

from . import util

class Notifier(Thread):
	"""
	A class to import notification modules and execute them in a seperate thread.

    Attributes
    ----------
    gordon : :class:`gordon_voice.GordonVoice`
        Instance of GordonVoice
    q : Queue
        Queue of notifications
    notificationClients : list(:meth:`gordon_voice.Notifier.NotificationClient`)
        List of notification clients
	"""
	class NotificationClient:
		"""
		Sub-class to keep a notification module's data.

        Attributes
        ----------
        module : Python module
            The python notification module
        lastTimeExecuted : int
            The last time the handle function of the module was called
        timestamp : int
            The timestamp of the last notification obtained from the module
        gordon_voice : :class:`gordon_voice.GordonVoice`
            Instance of GordonVoice
		"""
		def __init__(self,gordon_voice,moduleName,module,timestamp=None):
			self.moduleName = moduleName
			self.module = module
			self.lastTimeExecuted = None
			self.timestamp = timestamp
			self.gordon_voice = gordon_voice

		def run(self,qFunction):
			self.timestamp = self.module.handle(self.timestamp,self.gordon_voice.profile,qFunction)
			self.lastTimeExecuted = time.time()

	def __init__(self,gordon_voice):
		Thread.__init__(self)
		self.daemon = True
		self.gordon = gordon_voice
		self.q = queue.Queue()
		self.notificationClients = []

	def __conditionFunction(self,module):
		frequencyCond = (hasattr(module,'FREQUENCY') and type(module.FREQUENCY) == type(1))
		handleCond = (hasattr(module,'handle') and type(module.handle) == type(lambda x:x))
		return True if (frequencyCond and handleCond) else False

	def loadModule(self,filePath):
		"""
		Loads a module into the notifier.

		Parameters
		----------
		filePath : str
			Path to the python file to load.

		Raises
		------
		:exc:`gordon_voice.ModuleImportError`
			If there was an error while loading a module.
		"""

		filename = basename(filepath)
		try:
			module = util.loadModule(filename,filePath,self.__conditionFunction)
		except:
			raise
		else:
			if module:
				self.notificationClients.append(self.NotificationClient(self.gordon,filename,module))


	def loadModules(self,path=""):
		"""
		Loads notifications modules from path.

		Notification modules must have a FREQUENCY attribute (which is of int type),
		and a handle function which takes three arguments : a timestamp,
		the gordon profile, and a function to add notifications.
		The handle function must return the timestamp of the last notification.

		Parameters
		----------
		path : str
			The path to the notification modules directory.
			Default is the path set in your profile. If it's not set,
			gordon_voice will look in ./Modules/Notifications/
		"""

		path = path if path else self.gordon.profile['notificationModulesPath']
		modules = util.loadModulesFromDirectory(path,self.__conditionFunction)
		self.notificationClients = [self.NotificationClient(self.gordon,moduleName,module) \
		for moduleName,module in modules.items()]

	def run(self):
		"""
		Goes through every notification module and runs it depending on its frequency.
		"""

		while True:
			for notificationClient in self.notificationClients:
				if notificationClient.lastTimeExecuted == None or (time.time() - notificationClient.lastTimeExecuted) >= notificationClient.module.FREQUENCY:
					notificationClient.run(self.addToQueue)

	def addToQueue(self,notification):
		"""
		Adds a notification to the notifications queue.
		"""
		self.q.put(notification)

	def getNotification(self):
		"""
		Returns a notification. Note that this function is consuming.
		"""
		try:
			notif = self.q.get(block=False)
			return notif
		except queue.Empty:
			return None

	def getAllNotifications(self):
		"""
		Return a list of notifications in chronological order.
		Note that this function is consuming, so consecutive calls
		will yield different results.
		"""
		notifs = []

		notif = self.getNotification()

		while notif:
			notifs.append(notif)
			notif = self.getNotification()
		return notifs
