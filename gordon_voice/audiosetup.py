# -*- coding: utf-8 -*-

#####################
#				    #
#	10/12/2018		#
#	Victor Leveneur #
#					#
#####################

import pyaudio
import wave
import speech_recognition as sr


from ctypes import *
from contextlib import contextmanager
import pyaudio

ERROR_HANDLER_FUNC = CFUNCTYPE(None, c_char_p, c_int, c_char_p, c_int, c_char_p)

def py_error_handler(filename, line, function, err, fmt):
    pass

c_error_handler = ERROR_HANDLER_FUNC(py_error_handler)

@contextmanager
def noalsaerr():
    asound = cdll.LoadLibrary('libasound.so')
    asound.snd_lib_error_set_handler(c_error_handler)
    yield
    asound.snd_lib_error_set_handler(None)

"""
Some useful functions concerning the audio system.
"""


class WAVFileError(Exception):
	pass

def getDevicesList():
	with noalsaerr():
		p = pyaudio.PyAudio()
		return [p.get_device_info_by_index(i) for i in range(p.get_device_count())]

def getMicrophonesList():
	"""
	Get the list of microphones.

	Call speech_recognition's Microphone.list_microphone_names() function.

	Returns
	-------

	list(str)
		A list of microphones.
	"""
	with noalsaerr():
		return sr.Microphone.list_microphone_names()

def getActiveMicrophones():
	"""
	Get all the active microphones.

	Performs a test on each microphone and returns a dictionnary with each active microphone and its associated index.
	For a microphone to be listed it must not be silent, make sure that you mics are not muted.

	Returns
	-------
	dict[str,int]
		A dictionnary with each active microphone and its associated index.
	"""

	with noalsaerr():
		CHUNK = 512
		RECORD_SECONDS = 0.8
		p = pyaudio.PyAudio()
		devices = [p.get_device_info_by_index(i) for i in range(0,p.get_device_count())]
		activeMics = {}

		for d in devices:
			uselessDevices = ['pulse','default','sysdefault']
			if d['maxInputChannels'] != 0 and not d['name'].startswith('/') and d['name'] not in uselessDevices: # if there are input channels
				stream = p.open(format=pyaudio.paInt16,
						channels=1,
						rate=int(d['defaultSampleRate']),
						input=True,input_device_index=d['index'],
						frames_per_buffer=CHUNK)
				frames = []
				for i in range(0, int(int(d['defaultSampleRate']) / CHUNK * RECORD_SECONDS)):
					data = stream.read(CHUNK,exception_on_overflow = False)
					frames.append(data)
				stream.stop_stream()
				stream.close()

				isSilent = True
				for f in frames:
					isSilent = False if int.from_bytes(f,byteorder='big') != 0 else isSilent
				if not isSilent:
					activeMics[d['name']] = d['index']
		p.terminate()
		return activeMics

def getOutputSoundDevices():
	"""
	Lists the output devices.

	Returns
	-------
	dict[str,int]
		A dictionnary with each output device and its associated index.
	"""
	with noalsaerr():
		p = pyaudio.PyAudio()
		devices = [p.get_device_info_by_index(i) for i in range(0,p.get_device_count())]
		outputs = {}

		for d in devices:
			uselessDevices = ['pulse','default','sysdefault']
			if d['maxOutputChannels'] != 0 and not d['name'].startswith('/') and d['name'] not in uselessDevices:
				outputs[d['name']] = d['index']
		p.terminate()
		return outputs



def play(data,device=None,chunkSize=1024):
	"""
	Plays a WAV file.

	Plays a WAV file either from an actual file on the disk or from a file-like object.

	Parameters
	----------
	data : str or file-like object
		Path to the WAV file or file-like object containing the WAV audio data.
	device : int
		Optional.

		Index of the output device to use.

		Default is None (will use the default output device).
	chunkSize : int
		Optional.

		The chunk size.

		Default is 1024.
	Raises
	------
	:meth:`gordon_voice.WAVFileError`
		If there's an error while opening/decoding the WAV file.

	"""

	with noalsaerr():
		p = pyaudio.PyAudio()

		try:
			wf = wave.open(data, 'rb')
		except Exception as e:
			raise WAVFileError(str(e))
		else:
			stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
		                channels=wf.getnchannels(),
		                rate=wf.getframerate(),
		                output=True,output_device_index=device)

			data = wf.readframes(chunkSize)

			while len(data) > 0 :
				stream.write(data)
				data = wf.readframes(chunkSize)

			stream.stop_stream()
			stream.close()
		finally:
			p.terminate()
