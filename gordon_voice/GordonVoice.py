# -*- coding: utf-8 -*-

#####################
#				    #
#	02/12/2018		#
#	Victor Leveneur #
#					#
#####################

import speech_recognition as sr
from os.path import basename,exists
import os
import errno
import importlib.util
import yaml
import logging

from . import Speech
from . import Notifier
from . import util

class NoModuleHandleError(Exception):
	pass

class SafeExit(Exception):
	pass

class GordonVoice:

	"""
	A class to launch a module's function using Speech to Text technology.

	Attributes
	----------
	speech : :class:`gordon_voice.Speech`
		Instance of Speech (used to perform STT and TTS).
	modules : dict[str:Python module]
		Dictionnary with every python module and its name.
	modulesData : dict[str:Data]
		Dictionnary with runtime data of every modules.
	priorities : list[tuple(str,float)]
		List of tuples containing a module's name and its priority.
	profile : dict
		Dictionnary containing profile informations.
	notifier : :class:`gordon_voice.Notifier`
		Instance of notifier (used to launch notification modules).
	"""

	def __init__(self,profilePath=''):
		"""
		Instanciates Gordon.

		Parameters
		----------
		profilePath : str
			Optional.

			Path to the YAML file containing the profile's configuration.
		Returns
		-------
		:class:`gordon_voice.GordonVoice`
			A GordonVoice instance.
		"""
		self.logger = logging.getLogger(__name__)
		self.speech = Speech.Speech()
		self.modules = {}
		self.modulesData = {}
		self.priorities = []
		self.profile = {'language':'en-US','safeExitWord':'exit','modulesPath':'./Modules/',
		'notificationModulesPath':'./Modules/Notifications/','sttEngine':'google','ttsEngine':'google_speech','espeakArgs':None,
		'googleKey':None,'witKey':None,'bingKey':None,'googleCloudJSON':None,'houndifyIDs':{'id':'','key':''},
		'ibmIDs':{'id':'','key':''}}
		self.loadProfile(profilePath)
		self.notifier = Notifier.Notifier(self)
		self.notifier.loadModules()
		self.notifier.start()

	def loadProfile(self,path):
		"""
		Loads the profile file.

		This function will parse a YAML file to a python dictionnary (using yaml.safe_load).

		My advice is to specify gordon_voice's parameters in this file rather than in your code,
		so that you can be warned should there be any issues.
		You can also put your own modules parameters in this file and access them through
		:attr:`gordon_voice.GordonVoice.profile` (be sure not to used reserved parameters).

		Parameters you can specify in the profile file :

		+-------------------------+-------------------------+---------------------------------------------------------------+
		|          Label          |      Default value      |                          Description                          |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|         language        |          en-US          |                        Language to use                        |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|       safeExitWord      |           exit          | Word to say to raise :exc:`gordon_voice.SafeExit` exception   |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|       modulesPath       |        ./Modules/       |                        Path to modules                        |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		| notificationModulesPath | ./Modules/Notifications |                  Path to notification modules                 |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|        sttEngine        |          google         |                  Speech to text engine to use                 |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|        ttsEngine        |      google_speech      |                  Text to speech engine to use                 |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|        espeakArgs       |           None          |                        Espeak arguments                       |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|        googleKey        |           None          |               Key for google speech to text API               |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|          witKey         |           None          |                 Key for wit speech to text API                |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|         bingKey         |           None          |                Key for bing speech to text API                |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|     googleCloudJSON     |           None          |       Path to google_cloud's JSON authentification file       |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|       houndifyIDs       |    {'id':'','key':''}   |              Ids for houndify text to speech API              |
		+-------------------------+-------------------------+---------------------------------------------------------------+
		|          ibmIDs         |    {'id':'','key':''}   |                 Ids for ibm text to speech API                |
		+-------------------------+-------------------------+---------------------------------------------------------------+

		Check espeakArgs : :meth:`gordon_voice.Speech.tts`


		Parameters
		----------
		path : str
			Path to YAML file containing the profile's configuration.
		Raises
		------
		yaml.YAMLError
			If there's an error while loading the YAML file.
		:exc:`gordon_voice.FileNotFoundError`
			If the file doesn't exist.
		"""

		if not path:
			profile = {}
			self.logger.info("No profile filepath given, loading default profile")
		else:
			if exists(path):
				try:
					with open(path,'r') as f:
						profile = yaml.safe_load(f)
				except:
					self.logger.exception("Could not load profile at %s : ",path)
					raise
			else:
				self.logger.error("Could not open profile at %s",path)
				raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT),path)

		self.profile = {k : v for d in [self.profile,profile] for k,v in d.items()}

		if self.profile['ttsEngine'] == 'espeak' and not self.profile['espeakArgs']:
			self.logger.info("You're using espeak without any parameters. It is advised to apply parameters to your profile for a more human-like voice.\
			See the instructions on the documentation for more information.")

		if self.profile['googleCloudJSON'] and self.profile['sttEngine'] == 'google-cloud':
			if isinstance(self.profile['googleCloudJSON'],str):
				if exists(self.profile['googleCloudJSON']):
					try:
						with open(self.profile['googleCloudJSON'],'r') as creds:
							self.profile['googleCloudJSON'] = creds
					except:
						self.logger.warning("Google cloud credentials couldn't \
						be opened (%s)",self.profile['googleCloudJSON'])
				else:
					self.logger.warning("Google cloud credentials couldn't \
					be opened (%s)",self.profile['googleCloudJSON'])
			else:
				self.logger.warning('Wrong input for googleCloudJSON in profile file.')

		if self.profile['sttEngine'] not in ['sphinx','google','google-cloud','wit','bing','houndify','ibm']:
			self.profile['sttEngine'] = 'sphinx'
		else:
			msg = "{0:s} STT is used but no identification was given. {1:s}"
			if self.profile['sttEngine'] == 'google' and self.profile['googleKey'] == None:
				self.logger.warning(msg.format(self.profile['sttEngine'],'Using the default API key.'))
			elif self.profile['sttEngine'] == 'google-cloud' and self.profile['googleCloudJSON'] == None:
				self.logger.warning(msg.format(self.profile['sttEngine'],'Using the default API key.'))
			elif self.profile['sttEngine'] == 'wit' and self.profile['witKey'] == None:
				self.logger.warning(msg.format(self.profile['sttEngine'],\
				'This will cause an exception when trying to recognize speech.'))
			elif self.profile['sttEngine'] == 'bing' and self.profile['bingKey'] == None:
				self.logger.warning(msg.format(self.profile['sttEngine'],\
				'This will cause an exception when trying to recognize speech.'))
			elif self.profile['sttEngine'] == 'houndify' and self.profile['houndifyIDs'] == ('',''):
				self.logger.warning(msg.format(self.profile['sttEngine'],\
				'This will cause an exception when trying to recognize speech.'))
			elif self.profile['sttEngine'] == 'ibm' and self.profile['ibmIDs'] == ('',''):
				self.logger.warning(msg.format(self.profile['sttEngine'],\
				'This will cause an exception when trying to recognize speech.'))

	def __conditionFunction(self,module):
		wordsCond = (hasattr(module,'WORDS') and type(module.WORDS) == type([]))
		isValidCond = (hasattr(module,'isValid') and type(module.isValid) == type(lambda x:x))
		handleCond = (hasattr(module,'handle') and type(module.handle) == type(lambda x:x))


		return True if (wordsCond and isValidCond and handleCond) else False

	def __sortPriorities(self):
		self.priorities = sorted(self.priorities,key=lambda item:item[1],reverse=True)

	def loadModule(self,filePath,priority=1.0):
		"""
		Loads a module.

		Loads the module at `path`.

		Parameters
		----------
		filePath : str
			The path to the module.
		priority : float
			Optional.

			Module's priority.
			Default is 1.0 .

		Raises
		------
		:exc:`gordon_voice.ModuleImportError`
			If there was an error while importing the module.
		"""

		filename = basename(filepath)
		try:
			module = util.loadModule(filename,filePath,self.__conditionFunction)
		except:
			raise
		else:
			if module:
				self.modules[filename] = module
				self.modulesData[filename] = None
				self.prorities.append((filename,priority))
				self.__sortPriorities()

	def loadModules(self,path=''):
		"""
		Loads the modules.

		Loads the modules at `path`.
		This function will automatically look for a `priorities.yml` YAML file in the
		modules folder. This file can specify each module's priority like this : `myModule : myPriority` .
		If a module M1 has a higher priority than another module M2, and M1 and M2 have common words in their global variable WORDS,
		than M1 will be more prone to be called than M2 when saying those words.

		Parameters
		----------
		path : str
			Path to the directory containing all the modules.

		Raises
		------
		yaml.YAMLError
			If there's an error while loading the YAML file containing the modules priorities.
		"""

		path = path if path else self.profile['modulesPath']
		self.modules = util.loadModulesFromDirectory(path,self.__conditionFunction)
		self.modulesData = {moduleName : None for moduleName in self.modules.keys()}

		# Get modules priorities
		priorities = {}
		if exists(path+'prorities.yml'):
			try:
				with open(path+'prorities.yml','r'):
					priorities = yaml.safe_load(f)
			except:
				self.logger.exception("Error while loading %s",path+"priorities.yml :")
				raise
		priorities = {k:v for k,v in priorities.items() if k in self.modules}
		for k in self.modules.keys():
			if k not in priorities:
				priorities[k] = 1.0
		self.priorities = [(name,order) for name,order in priorities.items()]
		self.__sortPriorities()

	def activeListen(self,prefix="Gordon",params={}):
		"""
		Performs Speech to Text (STT).

		Performs STT and returns the converted text. If prefix is not empty, the result will be None if the text doesn't start with prefix.
		If the resulting text is the safe word then a SafeExit exception is thrown.

		Parameters
		----------
			prefix : str
				Optional.

				Prefix to identify speech directed to the module.

				Default is "Gordon".
			params : dict
				Optional.

				Additional settings for the STT operation.

				You can specify language,api,googleKey,googleCloudJSON,witKey,bingKey,houndifyIDs anbd ibmIDs.
				Please refer to :meth:`gordon_voice.Speech.tts` for more information.
		Returns
		-------
			str or None
				The recorded speech converted to text. Or None if there's a prefix and the text doesn't starts with the prefix.
		Raises
		------
		:exc:`gordon_voice.SafeExit`
			If the safe word has been given (only if there's no other text than the safe word).
		:exc:`speech_recognition.WaitTimeoutError`
			If no speech is detected before the set `timeout`.
		speech_recognition.RequestError
			If there is a problem with the API used (installation,authentification,operation or internet connection).
		speech_recognition.UnknownValueError
			If the speech is unintelligible.
		"""

		defaultParams = {'language':self.profile['language'],'api':self.profile['sttEngine']
		,'googleKey':self.profile['googleKey'],'googleCloudJSON':self.profile['googleCloudJSON']
		,'witKey':self.profile['witKey'],'bingKey':self.profile['bingKey'],
		'houndifyIDs':(self.profile['houndifyIDs']['id'],self.profile['houndifyIDs']['key']),
		'ibmIDs':(self.profile['ibmIDs']['id'],self.profile['ibmIDs']['key'])}
		try:
			result = self.speech.stt(**{k : v for d in [defaultParams,params] for k,v in d.items()})
		except:
			raise

		if len(result) >= len(self.profile['safeExitWord']) \
		and result[:len(self.profile['safeExitWord'])].lower() == self.profile['safeExitWord'].lower():
			raise SafeExit("Exit requested.")

		if prefix:
			if len(result) > len(prefix) and \
			result[:len(prefix)].lower() == prefix.lower() and result[len(prefix)] == ' ':
				return result[len(prefix)+1:]
			else:
				return None
		else:
			return result

	def tts(self,text,params={}):
		"""
		Performs Text to Speech (TTS).

		Performs TTS using Speech.tts function using profile preferences.

		Parameters
		----------
		text : str
			The text to convert to speech.
		params : dict
			Optional.

			Additional parameters for Speech.tts.
			Please refer to Speech.tss documentation for more information.

		Raises
		------
		:exc:`gordon_voice.GTTSError`
			If gTTS encountered an error while trying to perform TTS.
		:exc:`gordon_voice.GoogleCloudTTSError`
			If google-cloud TTS api encountered an error. It could be either an authentification error or a connection issue.
		:exc:`gordon_voice.WriteMP3Error`
			If the MP3 file could not be written.
		:exc:`gordon_voice.WAVFileError`
			If there's an error while trying to play the audio file.
		"""

		espeakArgs = {}
		if self.profile['espeakArgs']:
			espeakArgs = {'voice':None,'pitch':None,'volume':None,'speed':None,'delay':None}
			espeakData = {k:v for d in [espeakArgs,self.profile["espeakArgs"]] for k,v in d.items()}
			espeakArgs = {'v':espeakData['voice'],'p':espeakData['pitch'],'a':espeakData["volume"]\
			,'s':espeakData['speed'],'g':espeakData['delay']}
			espeakArgs = {k:v for k,v in espeakArgs.items() if v}

		defaultArgs = {'device':None,'lang':self.profile['language'],'api':self.profile['ttsEngine']
		,'espeakArgs':espeakArgs,'googleCloudArgs':{}}

		params = {k : v for d in [defaultArgs,params] for k,v in d.items()}

		try:
			self.speech.tts(text,**params)
		except:
			self.logger.exception("Error while performing TTS : ")
			raise

	def handle(self,text):
		"""
		Calls the `handle` function of a module if the text is valid.

		Loops through the modules by descending priority. If all the words in module.WORDS are \
		in the text and module.isValid(text) is true,then Gordon calls module.handle().

		A module's handle function must be formed as : `handle(profile,activeListen,tts,moduleData,changeModuleData,text)`

		* profile : gordon_voice's profile
		* activeListen : :meth:`gordon_voice.GordonVoice.activeListen`
		* tts : :meth:`gordon_voice.GordonVoice.tts`
		* moduleData : value of the data linked to this module (is initiated to None)
		* changeModuleData(data) : function to write module data
		* text : recognized text that lead to calling module's handle function

		Parameters
		----------
		text : str
			The text to test.

		Returns
		-------
		tuple(str,func)
			A tuple containing the name of the module called and the function to call it.
		Raises
		------
		:exc:`gordon_voice.NoModuleHandleError`
			If no module.handle() was called.
		"""

		text = text.lower()
		if text and len(self.modules) != 0:
			for item in self.priorities:
				module = self.modules[item[0]]
				if all(word in text for word in module.WORDS):
					if module.isValid(text):
						def changeModuleData(data):
							self.modulesData[item[0]] = data

						return (item[0],lambda : module.handle(self.profile,\
						self.activeListen,self.tts,self.modulesData[item[0]],changeModuleData,text))
		raise NoModuleHandleError('No module could be called.')
