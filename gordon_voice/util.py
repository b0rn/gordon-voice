# -*- coding: utf-8 -*-

#####################
#				    #
#	16/12/2018		#
#	Victor Leveneur #
#					#
#####################

from os import listdir
from os.path import isfile,isdir, join
import errno
import importlib.util

import logging

logger = logging.getLogger(__name__)

class ModuleImportError(Exception):
	pass

def loadModule(filename,path,conditionFunction):
	"""
	Load a module.

	Loads the module at `path`.

	Parameters
	----------
	filename : str
		The file's name (without the .py).
	path : str
		Path to the directory containing the module.
	conditionFunction : function
		A function which takes a module as parameter and returns
		True if the module complies to the defined rules.

	Returns
	-------
	Python module or None
		Returns the loaded python module if conditionFunction(module) is True.
		Else returns None.

	Raises
	------
	:exc:`gordon_voice.ModuleImportError`
		If there's a error while importing a module.
	"""
	try:
		spec = importlib.util.spec_from_file_location(filename,path+filename+'.py')
		tmp = importlib.util.module_from_spec(spec)
		spec.loader.exec_module(tmp)
	except Exception as e:
		logger.exception("Error encountered while loading module "+path+filename+".py : ")
		raise ModuleImportError("Error encountered while loading module "+path+filename+".py : "+str(e))
	else:
		return tmp if conditionFunction(tmp) else None

def loadModulesFromDirectory(path,conditionFunction):
	"""
	Loads modules in path.

	Loads the modules at `path`.

	Parameters
	----------
	path : str
		Path to the directory containing the modules.
	conditionFunction : function
		A function which takes a module as parameter and returns
		True if the module complies to the defined rules.

	Returns
	-------
	dict[str,Python module]
		Returns the loaded python module if conditionFunction(module) is True.
		Else returns None.
	"""

	if not isdir(path):
		return {}

	importlib.invalidate_caches()
	pythonFiles = [f for f in listdir(path) if (isfile(join(path, f)) and f.endswith('.py'))]
	ret = {}
	for f in pythonFiles:
		try:
			module = loadModule(f[:len(f)-3],path,conditionFunction)
			if module:ret[f[:len(f)-3]] = module
		except:
			pass
	return ret
